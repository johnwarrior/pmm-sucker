//
//  IMAPSuckerThread.cpp
//  PMM Sucker
//
//  Created by Juan V. Guerrero on 10/1/11.
//  Copyright (c) 2011 fn(x) Software. All rights reserved.
//

#include <iostream>
#include <map>
#include <time.h>
#include <sstream>
#include <poll.h>
#include "IMAPSuckerThread.h"
#include "ThreadDispatcher.h"
#include "MailMessage.h"
#include "libetpan/libetpan.h"
#include "GmailXOAuth.h"
#include <string.h>
#include <cstdlib>
#include "QuotaDB.h"

#ifndef DEFAULT_MAX_MAIL_FETCHERS
#define DEFAULT_MAX_MAIL_FETCHERS 3
#endif
#ifndef DEFAULT_FETCH_RETRY_INTERVAL
#define DEFAULT_FETCH_RETRY_INTERVAL 60
#endif
#ifndef DEFAULT_MAX_IMAP_CONNECTION_TIME
#define DEFAULT_MAX_IMAP_CONNECTION_TIME 1200
#endif
#ifndef DEFAULT_MAX_IMAP_IDLE_CONNECTION_TIME
#define DEFAULT_MAX_IMAP_IDLE_CONNECTION_TIME 1200
#endif
#ifndef DEFAULT_MAX_OLD_MESSAGES
#define DEFAULT_MAX_OLD_MESSAGES 1000
#endif
#ifndef DEFAULT_NON_IDLE_FETCH_MIN_INTERVAL
#define DEFAULT_NON_IDLE_FETCH_MIN_INTERVAL 10
#endif


namespace pmm {
	MTLogger imapLog;
	
	static const char *kGoogleIMAPNotAllowed1 = "[AUTH] Web login required: https://support.google.com/mail";
	static const char *kGoogleIMAPNotAllowed2 = "Please log in via your web browser: http://support.google.com/mail";
	
	static char * get_msg_att_msg_content(struct mailimap_msg_att * msg_att, size_t * p_msg_size, MailMessage &tm)
	{
		clistiter * cur;
		/* iterate on each result of one given message */
		for(cur = clist_begin(msg_att->att_list) ; cur != NULL ; cur = clist_next(cur)) {
			struct mailimap_msg_att_item * item = (struct mailimap_msg_att_item *)clist_content(cur);
			if (item->att_type != MAILIMAP_MSG_ATT_ITEM_STATIC) {
				continue;
			}
			if (item->att_data.att_static->att_type != MAILIMAP_MSG_ATT_BODY_SECTION) {
				continue;
			}
			* p_msg_size = item->att_data.att_static->att_data.att_body_section->sec_length;
			//MailMessage::parse(tm, item->att_data.att_static->att_data.att_body_section->sec_body_part);
			MailMessage::parse(tm, item->att_data.att_static->att_data.att_body_section->sec_body_part, item->att_data.att_static->att_data.att_body_section->sec_length);
			return item->att_data.att_static->att_data.att_body_section->sec_body_part;
		}
		return NULL;
	}
	
	static char * get_msg_content(clist * fetch_result, size_t * p_msg_size, MailMessage &tm)
	{
		clistiter * cur;
		
		/* for each message (there will be probably only on message) */
		for(cur = clist_begin(fetch_result) ; cur != NULL ; cur = clist_next(cur)) {
			struct mailimap_msg_att * msg_att;
			size_t msg_size;
			char * msg_content;
			
			msg_att = (struct mailimap_msg_att *)clist_content(cur);
			msg_content = get_msg_att_msg_content(msg_att, &msg_size, tm);
			if (msg_content == NULL) {
				continue;
			}
			
			* p_msg_size = msg_size;
			return msg_content;
		}
		
		return NULL;
	}
	
	static uint32_t get_uid(struct mailimap_msg_att * msg_att)
	{
		clistiter * cur;
		/* iterate on each result of one given message */
		for(cur = clist_begin(msg_att->att_list) ; cur != NULL ; cur = clist_next(cur)) {
			struct mailimap_msg_att_item * item;
			item = (struct mailimap_msg_att_item *)clist_content(cur);
			if (item->att_type != MAILIMAP_MSG_ATT_ITEM_STATIC) {
				continue;
			}
			if (item->att_data.att_static->att_type != MAILIMAP_MSG_ATT_UID) {
				continue;
			}
			return item->att_data.att_static->att_data.att_uid;
		}
		return 0;
	}
	
	int IMAPSuckerThread::MailFetcher::fetch_msg(struct mailimap * imap, uint32_t uid, SharedQueue<NotificationPayload> *notificationQueue, const IMAPSuckerThread::IMAPFetchControl &imapFetch)
	{
		if (fetchedMails.entryExists2(imapFetch.mailAccountInfo.email(), uid)) {
			//Do not fetch or even notify previously fetched e-mails
			return 0;
		}
		int retVal = 1;
		struct mailimap_set * set;
		struct mailimap_section * section;
		size_t msg_len;
		char * msg_content;
		struct mailimap_fetch_type * fetch_type;
		struct mailimap_fetch_att * fetch_att;
		int r;
		clist * fetch_result;
		
		set = mailimap_set_new_single(uid);
		fetch_type = mailimap_fetch_type_new_fetch_att_list_empty();
		section = mailimap_section_new(NULL);
		fetch_att = mailimap_fetch_att_new_body_peek_section(section);
		mailimap_fetch_type_new_fetch_att_list_add(fetch_type, fetch_att);
		
		r = mailimap_uid_fetch(imap, set, fetch_type, &fetch_result);
		if(etpanOperationFailed(r)){
			IMAPFetchControl ifc = imapFetch;
			ifc.madeAttempts++;
			ifc.nextAttempt = time(NULL) + fetchRetryInterval;
			fetchQueue->add(ifc);
		}
		else{
			MailMessage theMessage;
			theMessage.to = imapFetch.mailAccountInfo.email();
			std::stringstream msgUid_s;
			msgUid_s << (int)uid;
			theMessage.msgUid = msgUid_s.str();
			//Parse e-mail, retrieve FROM and SUBJECT
			msg_content = get_msg_content(fetch_result, &msg_len, theMessage);
			if (msg_content == NULL) {
				mailimap_fetch_list_free(fetch_result);
				return 0;
			}
			fetchedMails.addEntry2(imapFetch.mailAccountInfo.email(), uid);
			//Verify if theMessage is not too old, if it is then just discard it!!!
			if (theMessage.serverDate < threadStartTime) {
				imapLog << "Message(" << theMessage.msgUid << ") for " << imapFetch.mailAccountInfo.email() << " is too old, not notifying it!!!" << pmm::NL;
				retVal = -1;
			}
			else {
				std::vector<std::string> myDevTokens = imapFetch.mailAccountInfo.devTokens();
				for (size_t i = 0; i < myDevTokens.size(); i++) {
					//Apply all processing rules before notifying
					std::string alertTone;
					std::stringstream nMsg;
					nMsg << theMessage.from << "\n" << theMessage.subject;
					
					PreferenceEngine::defaultAlertTone(alertTone, imapFetch.mailAccountInfo.email()); //Here we retrieve the user alert tone
					
					NotificationPayload np(myDevTokens[i], nMsg.str(), imapFetch.badgeCounter, alertTone);
					np.origMailMessage = theMessage;
					notificationQueue->add(np);
					if(i == 0){
						if (!QuotaDB::decrease(imapFetch.mailAccountInfo.email())) {
							imapLog << "ATTENTION: Account " << imapFetch.mailAccountInfo.email() << " has ran out of quota!!!" << pmm::NL;
							pmm::NoQuotaNotificationPayload npi(myDevTokens[i], imapFetch.mailAccountInfo.email());
							if (imapFetch.mailAccountInfo.devel) {
								pmm::imapLog << "Using development notification queue for this message." << pmm::NL;
								develNotificationQueue->add(npi);
							}
							else notificationQueue->add(npi);
						}
						quotaUpdateVector->push_back(imapFetch.mailAccountInfo.email());
						pmmStorageQueue->add(np);
					}
				}
			}
			mailimap_fetch_list_free(fetch_result);
		}
		mailimap_fetch_att_free(fetch_att);
		mailimap_set_free(set);
		return retVal;
	}
	
	IMAPSuckerThread::IMAPFetchControl::IMAPFetchControl(){
		madeAttempts = 0;
		nextAttempt = 0;
		badgeCounter = 0;
		issuedDate = time(0);
	}
	
	IMAPSuckerThread::IMAPFetchControl::IMAPFetchControl(const IMAPFetchControl &ifc){
		mailAccountInfo = ifc.mailAccountInfo;
		nextAttempt = ifc.nextAttempt;
		madeAttempts = ifc.madeAttempts;
		badgeCounter += ifc.badgeCounter;
		issuedDate = ifc.issuedDate;
	}
	
	bool IMAPSuckerThread::IMAPFetchControl::operator==(const IMAPFetchControl &i) const {
		if (i.mailAccountInfo == mailAccountInfo) {
			return true;
		}
		return false;
	}
	
	bool IMAPSuckerThread::IMAPFetchControl::operator<(const IMAPFetchControl &i) const {
		if (i.mailAccountInfo.email().size() < i.mailAccountInfo.email().size()) {
			return true;
		}
		return false;
	}
	
	
	IMAPSuckerThread::IMAPControl::IMAPControl(){
		failedLoginAttemptsCount = 0;
		idling = false;
		imap = NULL;
		startedOn = time(NULL);
		maxIDLEConnectionTime = DEFAULT_MAX_IMAP_IDLE_CONNECTION_TIME;
		supportsIdle = true;
	}
	
	IMAPSuckerThread::IMAPControl::~IMAPControl(){
		
	}
	
	IMAPSuckerThread::MailFetcher::MailFetcher(){
		availableMessages = 0;
		pmmStorageQueue = NULL;
		threadStartTime = time(0) - 43200;
	}
	
	void IMAPSuckerThread::MailFetcher::operator()(){
		if (pmmStorageQueue == NULL) throw GenericException("Can't continue like this, the pmmStorageQueue is null!!!");
		pmm::imapLog << "DEBUG: IMAP MailFetcher warming up..." << pmm::NL;
		sleep(1);
		pmm::imapLog << "DEBUG: IMAP MailFetcher started!!!" << pmm::NL;
		fetchQueue->name = "IMAPFetchQueue";
		while (true) {
			IMAPFetchControl imapFetch;
			time_t startT = time(0);
			while (fetchQueue->extractEntry(imapFetch)) {
				if (busyEmails->contains(imapFetch.mailAccountInfo.email())) {
					if(startT % 600 == 0) imapLog << "WARNING: No need to monitor " << imapFetch.mailAccountInfo.email() << " in this thread, another thread is taking care of it." << pmm::NL;
					usleep(100);
					continue;
				}
				busyEmails->insert(imapFetch.mailAccountInfo.email());
				time_t rightNow = time(0);
				if (imapFetch.madeAttempts > 0 && rightNow < imapFetch.nextAttempt) {
					if (fetchQueue->size() == 0) {
						usleep(100);
					}
					fetchQueue->add(imapFetch);
				}
				else {
					while (mailboxPollBlocked == true) {
						sleep(1);
						if (time(0) % 30 == 0) {
							pmm::imapLog << "polling of " << imapFetch.mailAccountInfo.email() << " has been interrupted!!!" << pmm::NL;
						}
					}
#ifdef DEBUG_MSG_FETCH
					pmm::imapLog << "DEBUG: IMAP MailFetcher: Fetching messages for: " << imapFetch.mailAccountInfo.email() << pmm::NL;
#endif
					struct mailimap *imap = mailimap_new(0, NULL);
					int result;
					if (imapFetch.mailAccountInfo.useSSL()) {
						result = mailimap_ssl_connect(imap, imapFetch.mailAccountInfo.serverAddress().c_str(), imapFetch.mailAccountInfo.serverPort());
					}
					else {
						result = mailimap_socket_connect(imap, imapFetch.mailAccountInfo.serverAddress().c_str(), imapFetch.mailAccountInfo.serverPort());
					}
					if (etpanOperationFailed(result)) {
						imapFetch.madeAttempts++;
						imapFetch.nextAttempt = rightNow + fetchRetryInterval;
						fetchQueue->add(imapFetch);
#ifdef DEBUG
						if (imap->imap_response == 0) {
							pmm::imapLog << "CRITICAL: IMAP MailFetcher(" << (long)pthread_self() << ") Unable to connect to: " << imapFetch.mailAccountInfo.email() << ", etpan=" << result << " RE-SCHEDULING fetch!!!" << pmm::NL;
						}
						else {
							pmm::imapLog << "CRITICAL: IMAP MailFetcher(" << (long)pthread_self() << ") Unable to connect to: " << imapFetch.mailAccountInfo.email() << ", etpan=" << result << " , response=" << imap->imap_response << " RE-SCHEDULING fetch!!!" << pmm::NL;
						}
#endif
					}
					else {
						if (imapFetch.mailAccountInfo.usesOAuth) {
							GmailXOAuth gmailOAuth;
							std::string access_token = gmailOAuth.refreshToken(imapFetch.mailAccountInfo.password());
							result = mailimap_oauth2_authenticate(imap, imapFetch.mailAccountInfo.username().c_str(), access_token.c_str());
						}
						else {
							result = mailimap_login(imap, imapFetch.mailAccountInfo.username().c_str(), imapFetch.mailAccountInfo.password().c_str());
						}
						if(etpanOperationFailed(result)){
#warning TODO: Remember to report the user whenever we have too many login attempts
#ifdef DEBUG
							if(imap->imap_response == 0) pmm::imapLog << "CRITICAL: IMAP MailFetcher: Unable to login to: " << imapFetch.mailAccountInfo.email() << ", response=" << result << pmm::NL;
							else pmm::imapLog << "CRITICAL: IMAP MailFetcher: Unable to login to: " << imapFetch.mailAccountInfo.email() << ", response=" << imap->imap_response << pmm::NL;
#endif
							imapFetch.madeAttempts++;
							imapFetch.nextAttempt = rightNow + fetchRetryInterval;
							fetchQueue->add(imapFetch);
						}
						else {
							result = mailimap_select(imap, "INBOX");
							if (etpanOperationFailed(result)) {
								imapFetch.madeAttempts++;
								imapFetch.nextAttempt = time_t(NULL) + fetchRetryInterval;
								fetchQueue->add(imapFetch);
#ifdef DEBUG
								if(imap->imap_response == NULL)
									pmm::imapLog << "CRITICAL: IMAP MailFetcher: Unable to select INBOX(" << imapFetch.mailAccountInfo.email() << ") etpan error=" << result  << pmm::NL;
								else
									pmm::imapLog << "CRITICAL: IMAP MailFetcher: Unable to select INBOX(" << imapFetch.mailAccountInfo.email() << ") etpan error=" << result << " response=" << imap->imap_response << pmm::NL;
#endif
								
							}
							else {
								clist *unseenMails = clist_new();
								struct mailimap_search_key *sKey = mailimap_search_key_new(MAILIMAP_SEARCH_KEY_UNSEEN, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
								result = mailimap_uid_search(imap, (const char *)NULL, sKey, &unseenMails);
								if (result  != MAILIMAP_NO_ERROR) {
									if (imap->imap_response == 0) {
										imapLog << "CRITICAL: Can't find unseen messages, IMAP SEARCH failed miserably" << pmm::NL;
									}
									else {
										pmm::imapLog << "CRITICAL: MailFetcher: " << imapFetch.mailAccountInfo.email() << " SEARCH imap response=" << imap->imap_response << pmm::NL;
									}
								}
								else {
									imapFetch.madeAttempts = 0;
									imapFetch.badgeCounter = 0;
									int oldCount = 0;
									std::vector<uint32_t> uidSet;
									int numMessages = clist_count(unseenMails);
									//if(numMessages > 1000) pmm::imapLog << "WARNING: " << imapFetch.mailAccountInfo.email() << " is very large, contains " << numMessages << " e-mails stored" << pmm::NL;
									time_t fetchT0 = time(0);
									for(clistiter * cur = clist_begin(unseenMails) ; cur != NULL ; cur = clist_next(cur)) {
										if (!QuotaDB::have(imapFetch.mailAccountInfo.email())) {
											pmm::imapLog << imapFetch.mailAccountInfo.email() << " has ran out of quota in the middle of a IMAP mailbox poll!!!" << pmm::NL;
											break;
										}
										uint32_t uid;
										uid = *((uint32_t *)clist_content(cur));
#ifdef DEBUG_IMAP
										pmm::imapLog << "DEBUG: IMAP MailFetcher " << imapFetch.mailAccountInfo.email() << " got UID=" << (int)uid << pmm::NL;
#endif
										imapFetch.badgeCounter++;
										int gotMessage = 0;
										if (imapFetch.mailAccountInfo.devel) {
											//pmm::imapLog << "Using development notification queue..." << pmm::NL;
											gotMessage = fetch_msg(imap, uid, develNotificationQueue, imapFetch);
										}
										else {
											gotMessage = fetch_msg(imap, uid, myNotificationQueue, imapFetch);
										}
										uidSet.push_back(uid);
										if (gotMessage > 0) {
											int cnt = cntRetrieved->get() + 1;
											cntRetrieved->set(cnt);
										}
										else if(gotMessage < 0){
											if(++oldCount > DEFAULT_MAX_OLD_MESSAGES || (time(0) - fetchT0) > 900){
												pmm::imapLog << "WARNING: " << imapFetch.mailAccountInfo.email() << " has too many old messages, looks like this user has never ever seen his inbox." << pmm::NL;
												pmm::imapLog << "WARNING Adding all " << numMessages << " to " << imapFetch.mailAccountInfo.email() << " seen messages database :-(" << pmm::NL;
												cur = clist_next(cur);
												std::string theEmail = imapFetch.mailAccountInfo.email();
												while (cur != NULL) {
													uid = *((uint32_t *)clist_content(cur));
													std::stringstream msgUid_s;
													msgUid_s << (int)uid;
													fetchedMails.addEntry2(theEmail, msgUid_s.str());
													cur = clist_next(cur);
												}
											}
										}
									}
									//Remove old entries if the current time is a multiple of 60 seconds
									if (rightNow % 600 == 0) {
#ifdef DEBUG
										pmm::imapLog << "Removing old uid entries from fetch control database..." << pmm::NL;
#endif
										fetchedMails.removeEntriesNotInSet2(imapFetch.mailAccountInfo.email(), uidSet);
									}
									mailimap_search_result_free(unseenMails);
								}
							}
							mailimap_logout(imap);
						}
						mailimap_close(imap);
					}
					mailimap_free(imap);
				}
				busyEmails->erase(imapFetch.mailAccountInfo.email());
			}
			time_t endT = time(0);
			if(startT != endT) usleep(1000);
			else usleep(10000);
		}
	}
	
	
	IMAPSuckerThread::IMAPControl::IMAPControl(const IMAPControl &ic){
		failedLoginAttemptsCount = ic.failedLoginAttemptsCount;
		imap = ic.imap;
	}
	
	
	IMAPSuckerThread::IMAPSuckerThread(){
		maxMailFetchers = DEFAULT_MAX_MAIL_FETCHERS;
		mailFetchers = new MailFetcher[maxMailFetchers];
		//IMAPSuckerThread::fetchedMails.expireOldEntries();
		threadStartTime = time(0) - 43200;
	}
	IMAPSuckerThread::IMAPSuckerThread(size_t _maxMailFetchers){
		maxMailFetchers = _maxMailFetchers;
		mailFetchers = new MailFetcher[maxMailFetchers];
		//IMAPSuckerThread::fetchedMails.expireOldEntries();
		threadStartTime = time(0) - 43200;
	}
	IMAPSuckerThread::~IMAPSuckerThread(){
		delete [] mailFetchers;
	}
	
	void IMAPSuckerThread::closeConnection(const MailAccountInfo &m){
		std::string theEmail = m.email();
		if (mailboxControl[theEmail].isOpened) {
			if (imapControl[theEmail].supportsIdle) {
				struct mailimap *imap = imapControl[theEmail].imap;
				mailimap_close(imap);
				mailimap_free(imapControl[theEmail].imap);
				imapControl[theEmail].imap = NULL;
			}
		}
		mailboxControl[theEmail].isOpened = false;
	}
	
	void IMAPSuckerThread::openConnection(const MailAccountInfo &m){
		int result;
		std::string theEmail = m.email();
		if(nextConnectAttempt[theEmail] > time(0)) return;
		for (size_t i = 0; i < maxMailFetchers; i++) {
			if (mailFetchers[i].isRunning == false) {
				mailFetchers[i].myNotificationQueue = notificationQueue;
				mailFetchers[i].develNotificationQueue = develNotificationQueue;
				mailFetchers[i].fetchQueue = &imapFetchQueue;
				mailFetchers[i].quotaUpdateVector = quotaUpdateVector;
				mailFetchers[i].pmmStorageQueue = pmmStorageQueue;
				mailFetchers[i].threadStartTime = threadStartTime;
				mailFetchers[i].busyEmails = &busyEmails;
				mailFetchers[i].cntRetrieved = &cntRetrievedMessages;
				pmm::ThreadDispatcher::start(mailFetchers[i], 8 * 1024 * 1024);
			}
		}
		if(imapControl[theEmail].imap == NULL) imapControl[theEmail].imap = mailimap_new(0, NULL);
		if (serverConnectAttempts.find(m.serverAddress()) == serverConnectAttempts.end()) serverConnectAttempts[m.serverAddress()] = 0;
#ifdef DEBUG_IMAP_CONNECT
		imapLog << "Connecting to " << theEmail << " port " << m.serverPort() << pmm::NL;
#endif
		if (m.useSSL()) {
			result = mailimap_ssl_connect(imapControl[theEmail].imap, m.serverAddress().c_str(), m.serverPort());
		}
		else {
			result = mailimap_socket_connect(imapControl[theEmail].imap, m.serverAddress().c_str(), m.serverPort());
		}
		if (etpanOperationFailed(result)) {
			time_t now = time(0);
			serverConnectAttempts[m.serverAddress()] = serverConnectAttempts[m.serverAddress()] + 1;
			if (serverConnectAttempts[m.serverAddress()] > maxServerReconnects) {
				//Max reconnect exceeded, notify user
				std::stringstream errmsg;
				errmsg << "Unable to connect to server " << m.serverAddress() << ", port=" << m.serverPort() << ", ssl=";
				if(m.useSSL()){
					errmsg << "ON";
				}
				else {
					errmsg << "OFF";
				}
				errmsg << " we will retry in aproximately 1 hour.\nPerhaps something might be wrong with " << m.email() << " account settings.";
				pmm::imapLog << "PANIC: " << errmsg.str() << pmm::NL;
				serverConnectAttempts[m.serverAddress()] = 0;
				mailboxControl[theEmail].lastCheck = now + 3600;
				nextConnectAttempt[theEmail] = now + 3600;
				scheduleFailureReport(m, errmsg.str(), false);
			}
			else {
				nextConnectAttempt[theEmail] = now + 30 + (90 % serverConnectAttempts[m.serverAddress()]);
			}
			mailboxControl[theEmail].isOpened = false;
			mailimap_free(imapControl[theEmail].imap);
			imapControl[theEmail].imap = NULL;
		}
		else {
			mailboxControl[theEmail].openedOn = time(NULL);
			//Proceed to login stage
			if (m.usesOAuth) {
				//Retrieve an access token first!!!!!
				GmailXOAuth gmailCtrl;
#ifdef DEBUG
				pmm::imapLog << "Refreshing access token for " << m.email() << "..." << pmm::NL;
#endif
				std::string accessToken = gmailCtrl.refreshToken(m.password());
				result = mailimap_oauth2_authenticate(imapControl[theEmail].imap, m.username().c_str(), accessToken.c_str());
#ifdef DEBUG
				if (imapControl[theEmail].imap->imap_response != NULL) {
					pmm::imapLog << "OAuth2(" << m.email() << ") authentication result: " << result << ": " << imapControl[theEmail].imap->imap_response << pmm::NL;
				}
				else {
					pmm::imapLog << "OAuth2(" << m.email() << ") authentication result: " << result << pmm::NL;
				}
#endif
			}
			else {
				result = mailimap_login(imapControl[theEmail].imap, m.username().c_str(), m.password().c_str());
			}
			if(etpanOperationFailed(result)){
				int attempt = serverConnectAttempts[m.serverAddress()] + 1;
				serverConnectAttempts[m.serverAddress()] = attempt;
				int f_tmp = cntFailedLoginAttempts.get() + 1;
				cntFailedLoginAttempts = f_tmp;
				time_t now = time(0);
#ifdef DEBUG
				if(now % 60 == 0){
					if(imapControl[theEmail].imap->imap_response == 0) pmm::imapLog << "CRITICAL: (" << attempt << ") Unable to login to: " << m.email() << ", response=" << result << pmm::NL;
					else pmm::imapLog << "CRITICAL: (" << attempt << ") Unable to login to: " << m.email() << ", response=" << imapControl[theEmail].imap->imap_response << pmm::NL;
				}
#endif
				int multiplier = attempt / maxServerReconnects + 1;
				if (m.serverAddress().compare("imap.gmail.com") == 0) {
					if (imapControl[theEmail].imap->imap_response != 0) {
						if (strncasecmp(imapControl[theEmail].imap->imap_response, kGoogleIMAPNotAllowed1, 57) == 0 ||
							strncasecmp(imapControl[theEmail].imap->imap_response, kGoogleIMAPNotAllowed2, 57) == 0) {
							multiplier = multiplier + 12;
#ifdef DEBUG
							pmm::imapLog << "PANIC: Google requested web login for account " << theEmail << " not retrying until at least " << multiplier << " hours." << pmm::NL;
#endif
							std::string gotIt;
							if (!localConfig->get(gotIt, m.email(), "gNotified")) {
								gmailAuthRequestedQ->add(theEmail);
								std::vector<std::string> _allTokens = m.devTokens();
								for (size_t k = 0; k < _allTokens.size(); k++) {
									NotificationPayload theMsg(_allTokens[k], "In order to poll this e-mail account additional authorization from Gmail is required.");
									theMsg.customParams["g"] = "1";
									if (m.devel) {
										develNotificationQueue->add(theMsg);
									}
									else {
										notificationQueue->add(theMsg);
									}
								}
							}
							time_t nextRetry = now + 3600 + rand() % 300;
							nextConnectAttempt[theEmail] = nextRetry;
							mailboxControl[theEmail].lastCheck = nextRetry;
							mailboxControl[theEmail].isOpened = false;
							mailimap_close(imapControl[theEmail].imap);
							mailimap_free(imapControl[theEmail].imap);
							imapControl[theEmail].imap = NULL;
							return;
						}
					}
				}
				if (attempt > maxServerReconnects) {
					//Max reconnect exceeded, notify user
					std::stringstream errmsg;
					errmsg << "Unable to login to " << theEmail;
					bool gmailAuthReq = false;
					if(imapControl[theEmail].imap->imap_response == 0) {
						errmsg << ", please check your app settings and/or IMAP credentials information.";
					}
					else {
						std::string respx = imapControl[theEmail].imap->imap_response;
						if (respx.find("Web login required: https://support.google.") == respx.npos) {
							
							errmsg << " due to: " << imapControl[theEmail].imap->imap_response << "\nCheck your app settings.";
						}
						else {
							multiplier = 1;
							errmsg.str(std::string());
							errmsg << "Additional Gmail authorization is required, check your inbox to authorize Push Me Mail.";
							//Send an e-mail to the user explaining
							gmailAuthReq = true;
						}
					}
					multiplier = 24;
					errmsg << "\nWe will re-attempt to login in at least " << multiplier << " hours.";
					scheduleFailureReport(m, errmsg.str(), gmailAuthReq);
					
					int rndTime = now % 300;
					nextConnectAttempt[theEmail] = now + 3600 * multiplier + rndTime;
					mailboxControl[theEmail].lastCheck = now + 3600 * multiplier + rndTime;
					mailboxControl[theEmail].isOpened = false;
				}
				else {
					nextConnectAttempt[theEmail] = now + 30;
					mailboxControl[theEmail].lastCheck = now + 30;
				}
				mailimap_close(imapControl[theEmail].imap);
				mailimap_free(imapControl[theEmail].imap);
				imapControl[theEmail].imap = NULL;
				mailboxControl[theEmail].isOpened = false;
			}
			else {
				//Start IMAP IDLE processing...
#ifdef DEBUG
				pmm::imapLog << "IMAPSuckerThread(" << (long)pthread_self() << "): Starting IMAP IDLE for " << theEmail << pmm::NL;
#endif
				//mailstream_debug = 1;
				result = mailimap_select(imapControl[theEmail].imap, "INBOX");
				if(etpanOperationFailed(result)){
					if (imapControl[theEmail].imap->imap_response == 0) {
						pmm::imapLog << "FATAL: Unable to select INBOX folder in account " << theEmail << pmm::NL;
					}
					else {
						pmm::imapLog << "FATAL: Unable to select INBOX folder in account " << theEmail << ": " << imapControl[theEmail].imap->imap_response <<  pmm::NL;
					}
					//throw GenericException("Unable to select INBOX folder");
					mailimap_close(imapControl[theEmail].imap);
					mailimap_free(imapControl[theEmail].imap);
					imapControl[theEmail].imap = NULL;
					mailboxControl[theEmail].isOpened = false;
					time_t now = time(0);
					nextConnectAttempt[theEmail] = now + 60 * serverConnectAttempts[m.serverAddress()];
					mailboxControl[theEmail].lastCheck = nextConnectAttempt[theEmail];
				}
				else {
					nextConnectAttempt[theEmail] = 0;
					serverConnectAttempts[m.serverAddress()] = 0;

					int idleEnabled;
#ifdef ENABLE_NATE_DOT_COM_WORKAROUND
					if (theEmail.find("@nate.com") != theEmail.npos) {
						idleEnabled = 0;
					}
					else
#endif
					idleEnabled = mailimap_has_idle(imapControl[theEmail].imap);
					if(!idleEnabled){
						imapLog << "WARNING: " << theEmail << " is not hosted in an IMAP IDLE environment." << pmm::NL;
						mailboxControl[theEmail].isOpened = true;
						mailboxControl[theEmail].openedOn = time(0x00);
						imapControl[theEmail].startedOn = time(NULL);
						mailimap_logout(imapControl[theEmail].imap);
						mailimap_close(imapControl[theEmail].imap);
						mailimap_free(imapControl[theEmail].imap);
						imapControl[theEmail].imap = NULL;
						imapControl[theEmail].supportsIdle = false;
						return;
					}
					//Manually check mailbox in case any unnotified e-mail arrived before a call to IDLE
					fetchMails(m);
					imapControl[theEmail].supportsIdle = true;
					result = mailimap_idle(imapControl[theEmail].imap);
					if(result != MAILIMAP_NO_ERROR){
						if(imapControl[theEmail].imap->imap_response == 0){
							pmm::imapLog << "CRITICAL: Account " << m.email() << " said it was hosted at an IMAP IDLE capable server but we failed to monitor it!!!" << pmm::NL;
						}
						else {
							pmm::imapLog << "CRITICAL: Account " << m.email() << " said it was hosted at an IMAP IDLE capable server but we failed to monitor it: " << imapControl[theEmail].imap->imap_response << pmm::NL;
						}
						//throw GenericException("Unable to start IDLE!!!");
						mailboxControl[theEmail].isOpened = false;
						mailimap_logout(imapControl[theEmail].imap);
						mailimap_close(imapControl[theEmail].imap);
						mailimap_free(imapControl[theEmail].imap);
						imapControl[theEmail].imap = NULL;
					}
					else {
						//Report successfull login
						mailboxControl[theEmail].isOpened = true;
						mailboxControl[theEmail].openedOn = time(0x00);
						imapControl[theEmail].startedOn = time(NULL);
						//sleep(1);
#ifdef DEBUG
						pmm::imapLog << theEmail << " is being succesfully monitored!!!" << pmm::NL;
#endif
					}
				}
			}
		}
	}
	
	void IMAPSuckerThread::checkEmail(const MailAccountInfo &m){
		std::string theEmail = m.email();
		if (mailboxControl[theEmail].isOpened) {
			time_t rightNow = time(0);
			if (!imapControl[theEmail].supportsIdle) {
				if(rightNow - mailboxControl[theEmail].lastCheck > DEFAULT_NON_IDLE_FETCH_MIN_INTERVAL){
#ifdef DEBUG
					if(rightNow % 600 == 0) pmm::imapLog << "DEBUG: IMAP MailFetcher: Fetching messages for non-idle account: " << theEmail << pmm::NL;
#endif
					fetchMails(m);
				}
				return;
			}
			mailimap *imap = imapControl[theEmail].imap;
			if (imapControl[theEmail].startedOn + DEFAULT_MAX_IMAP_CONNECTION_TIME < rightNow) {
				//Think about closing and re-opening this connection!!!
#ifdef DEBUG
				pmm::imapLog << "Max connection time account for " << theEmail << " exceeded (" << DEFAULT_MAX_IMAP_CONNECTION_TIME << " seconds) dropping connection!!!" << pmm::NL;
#endif
				//mailimap_idle_done(imap);
				//mailimap_logout(imap);
				if (imap->imap_stream) {
					mailstream_close(imap->imap_stream);
					imap->imap_stream = NULL;
				}
				mailimap_close(imap);
				mailimap_free(imap);
				imapControl[theEmail].imap = NULL;
				mailboxControl[theEmail].isOpened = false;
				return;
			}
			mailstream_low *mlow = mailstream_get_low(imap->imap_stream);
			int fd = mailstream_low_get_fd(mlow);
			if(fd < 0) throw GenericException("Unable to get a valid FD, check libetpan compilation flags, make sure no cfnetwork support have been cooked in.");
			struct pollfd pelem;
			pelem.fd = fd;
			pelem.events = POLLIN;
			int recent = -1;
			bool resetIdle = false;
			int antiLoopCounter = 0;
			while (poll(&pelem, 1, 0) > 0) {
				char *response = mailimap_read_line(imap);
				if(response != NULL){
#ifdef DEBUG_MSG_FETCH
					pmm::imapLog << "DEBUG: IMAPSuckerThread(" << (long)pthread_self() << ") IDLE GOT response=" << response << " for " << theEmail << pmm::NL;
#endif
				}
				else {
					pmm::imapLog << "CRITICAL: IMAPSuckerThread(" << (long)pthread_self() << ") IDLE GOT response=NULL for " << theEmail << ", re-connecting..." << pmm::NL;
					if (imap->imap_stream) {
						mailstream_close(imap->imap_stream);
						imap->imap_stream = NULL;
					}
					mailimap_close(imap);
					mailimap_free(imap);
					imapControl[theEmail].imap = NULL;
					imapControl[theEmail].failedLoginAttemptsCount = 0;
					mailboxControl[theEmail].isOpened = false;
					return;
				}
				if (response != NULL && strlen(response) > 0 && strstr(response, "OK Still") == NULL) {
					if (strstr(response, "EXPUNGE") != NULL) {
#warning Expire old messages here
					}
					else {
						//Compute how many recent we have
						//std::istringstream input(std::string(response).substr(2));
						//input >> recent;
						resetIdle = true;
						mailboxControl[theEmail].availableMessages += recent;
#ifdef DEBUG_MSG_FETCH
						pmm::imapLog << "DEBUG: IDLE GOT recent=" << recent << " for " << theEmail << pmm::NL;
#endif
						fetchMails(m);
					}
				}
				if(response == NULL) break;
				if(antiLoopCounter > 0 && antiLoopCounter % 2 == 0) usleep(1000); //Sleep 1 milliseconds se we don't starve our CPU
				if(antiLoopCounter++ > 100) {
					pmm::imapLog << "PANIC: Got a looped IDLE status checker for " << theEmail << ", attempting to send an IDLE reset to break free!!!" << pmm::NL;
					resetIdle = true;
					fetchMails(m);
					break;
				}
			}
			if (resetIdle) {
				int result = mailimap_idle_done(imap);
				if(result != MAILIMAP_NO_ERROR){
					if(imap->imap_response == NULL){
						pmm::imapLog << "CRITICAL: Unable to send DONE to IMAP after IDLE for: " << theEmail << " disconnecting from monitoring, we will reconnect in the next cycle" << pmm::NL;
					}
					else {
						pmm::imapLog << "CRITICAL: Unable to send DONE to IMAP after IDLE for: " << theEmail << " disconnecting from monitoring, we will reconnect in the next cycle, response: " << imap->imap_response << pmm::NL;
					}
					mailimap_idle_done(imap);
					mailimap_logout(imap);
					mailimap_close(imap);
					mailimap_free(imap);
					imapControl[theEmail].imap = NULL;
					mailboxControl[theEmail].isOpened = false;
				}
				else {
					result = mailimap_idle(imap);
					if(result != MAILIMAP_NO_ERROR){
						//throw GenericException("Unable to restart IDLE after DONE.");
						if(imap->imap_response == NULL) pmm::imapLog << "CRITICAL: unable to reset IDLE, perhaps this is not the time to send such command?, disconnecting..." << pmm::NL;
						else pmm::imapLog << "CRITICAL: unable to reset IDLE: " << imap->imap_response << pmm::NL;
						mailimap_logout(imap);
						mailimap_close(imap);
						mailimap_free(imap);
						imapControl[theEmail].imap = NULL;
						mailboxControl[theEmail].isOpened = false;
					}
					else {
						fetchMails(m);
					}
				}
			}
		}
	}
	
	void IMAPSuckerThread::fetchMails(const MailAccountInfo &m){
		//Find and schedule a fetching thread
		IMAPFetchControl ifc;
		ifc.mailAccountInfo = m;
		ifc.issuedDate = time(0);
		imapFetchQueue.add(ifc);
	}
}