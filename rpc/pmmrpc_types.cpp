/**
 * Autogenerated by Thrift Compiler (0.9.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#include "pmmrpc_types.h"

#include <algorithm>

namespace pmmrpc {

const char* Command::ascii_fingerprint = "F7C641917C22B35AE581CCD54910B00D";
const uint8_t Command::binary_fingerprint[16] = {0xF7,0xC6,0x41,0x91,0x7C,0x22,0xB3,0x5A,0xE5,0x81,0xCC,0xD5,0x49,0x10,0xB0,0x0D};

uint32_t Command::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->name);
          this->__isset.name = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_MAP) {
          {
            this->parameter.clear();
            uint32_t _size0;
            ::apache::thrift::protocol::TType _ktype1;
            ::apache::thrift::protocol::TType _vtype2;
            xfer += iprot->readMapBegin(_ktype1, _vtype2, _size0);
            uint32_t _i4;
            for (_i4 = 0; _i4 < _size0; ++_i4)
            {
              std::string _key5;
              xfer += iprot->readString(_key5);
              std::string& _val6 = this->parameter[_key5];
              xfer += iprot->readString(_val6);
            }
            xfer += iprot->readMapEnd();
          }
          this->__isset.parameter = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t Command::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("Command");

  xfer += oprot->writeFieldBegin("name", ::apache::thrift::protocol::T_STRING, 1);
  xfer += oprot->writeString(this->name);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("parameter", ::apache::thrift::protocol::T_MAP, 2);
  {
    xfer += oprot->writeMapBegin(::apache::thrift::protocol::T_STRING, ::apache::thrift::protocol::T_STRING, static_cast<uint32_t>(this->parameter.size()));
    std::map<std::string, std::string> ::const_iterator _iter7;
    for (_iter7 = this->parameter.begin(); _iter7 != this->parameter.end(); ++_iter7)
    {
      xfer += oprot->writeString(_iter7->first);
      xfer += oprot->writeString(_iter7->second);
    }
    xfer += oprot->writeMapEnd();
  }
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(Command &a, Command &b) {
  using ::std::swap;
  swap(a.name, b.name);
  swap(a.parameter, b.parameter);
  swap(a.__isset, b.__isset);
}

const char* FetchDBItem::ascii_fingerprint = "3368C2F81F2FEF71F11EDACDB2A3ECEF";
const uint8_t FetchDBItem::binary_fingerprint[16] = {0x33,0x68,0xC2,0xF8,0x1F,0x2F,0xEF,0x71,0xF1,0x1E,0xDA,0xCD,0xB2,0xA3,0xEC,0xEF};

uint32_t FetchDBItem::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->timestamp);
          this->__isset.timestamp = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->email);
          this->__isset.email = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->uid);
          this->__isset.uid = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t FetchDBItem::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("FetchDBItem");

  xfer += oprot->writeFieldBegin("timestamp", ::apache::thrift::protocol::T_I32, 1);
  xfer += oprot->writeI32(this->timestamp);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("email", ::apache::thrift::protocol::T_STRING, 2);
  xfer += oprot->writeString(this->email);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("uid", ::apache::thrift::protocol::T_STRING, 3);
  xfer += oprot->writeString(this->uid);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(FetchDBItem &a, FetchDBItem &b) {
  using ::std::swap;
  swap(a.timestamp, b.timestamp);
  swap(a.email, b.email);
  swap(a.uid, b.uid);
  swap(a.__isset, b.__isset);
}

const char* FetchDBInitialSyncItem::ascii_fingerprint = "25702B8D5E28AA39160F267DABBC8446";
const uint8_t FetchDBInitialSyncItem::binary_fingerprint[16] = {0x25,0x70,0x2B,0x8D,0x5E,0x28,0xAA,0x39,0x16,0x0F,0x26,0x7D,0xAB,0xBC,0x84,0x46};

uint32_t FetchDBInitialSyncItem::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->email);
          this->__isset.email = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_LIST) {
          {
            this->uids.clear();
            uint32_t _size8;
            ::apache::thrift::protocol::TType _etype11;
            xfer += iprot->readListBegin(_etype11, _size8);
            this->uids.resize(_size8);
            uint32_t _i12;
            for (_i12 = 0; _i12 < _size8; ++_i12)
            {
              xfer += iprot->readString(this->uids[_i12]);
            }
            xfer += iprot->readListEnd();
          }
          this->__isset.uids = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t FetchDBInitialSyncItem::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("FetchDBInitialSyncItem");

  xfer += oprot->writeFieldBegin("email", ::apache::thrift::protocol::T_STRING, 1);
  xfer += oprot->writeString(this->email);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("uids", ::apache::thrift::protocol::T_LIST, 2);
  {
    xfer += oprot->writeListBegin(::apache::thrift::protocol::T_STRING, static_cast<uint32_t>(this->uids.size()));
    std::vector<std::string> ::const_iterator _iter13;
    for (_iter13 = this->uids.begin(); _iter13 != this->uids.end(); ++_iter13)
    {
      xfer += oprot->writeString((*_iter13));
    }
    xfer += oprot->writeListEnd();
  }
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(FetchDBInitialSyncItem &a, FetchDBInitialSyncItem &b) {
  using ::std::swap;
  swap(a.email, b.email);
  swap(a.uids, b.uids);
  swap(a.__isset, b.__isset);
}

const char* NotificationPayload::ascii_fingerprint = "194A357BC9EE908DC4763D0A5147760A";
const uint8_t NotificationPayload::binary_fingerprint[16] = {0x19,0x4A,0x35,0x7B,0xC9,0xEE,0x90,0x8D,0xC4,0x76,0x3D,0x0A,0x51,0x47,0x76,0x0A};

uint32_t NotificationPayload::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->devtoken);
          this->__isset.devtoken = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->message);
          this->__isset.message = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->sound);
          this->__isset.sound = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 4:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->badge);
          this->__isset.badge = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t NotificationPayload::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("NotificationPayload");

  xfer += oprot->writeFieldBegin("devtoken", ::apache::thrift::protocol::T_STRING, 1);
  xfer += oprot->writeString(this->devtoken);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("message", ::apache::thrift::protocol::T_STRING, 2);
  xfer += oprot->writeString(this->message);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("sound", ::apache::thrift::protocol::T_STRING, 3);
  xfer += oprot->writeString(this->sound);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("badge", ::apache::thrift::protocol::T_I32, 4);
  xfer += oprot->writeI32(this->badge);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(NotificationPayload &a, NotificationPayload &b) {
  using ::std::swap;
  swap(a.devtoken, b.devtoken);
  swap(a.message, b.message);
  swap(a.sound, b.sound);
  swap(a.badge, b.badge);
  swap(a.__isset, b.__isset);
}

const char* MailAccountInfo::ascii_fingerprint = "48C5E37CB8D6243BAEFBE58A57200C77";
const uint8_t MailAccountInfo::binary_fingerprint[16] = {0x48,0xC5,0xE3,0x7C,0xB8,0xD6,0x24,0x3B,0xAE,0xFB,0xE5,0x8A,0x57,0x20,0x0C,0x77};

uint32_t MailAccountInfo::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->email);
          this->__isset.email = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->mailboxType);
          this->__isset.mailboxType = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->username);
          this->__isset.username = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 4:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->password);
          this->__isset.password = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 5:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->serverAddress);
          this->__isset.serverAddress = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 6:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->serverPort);
          this->__isset.serverPort = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 7:
        if (ftype == ::apache::thrift::protocol::T_BOOL) {
          xfer += iprot->readBool(this->useSSL);
          this->__isset.useSSL = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 8:
        if (ftype == ::apache::thrift::protocol::T_LIST) {
          {
            this->devTokens.clear();
            uint32_t _size14;
            ::apache::thrift::protocol::TType _etype17;
            xfer += iprot->readListBegin(_etype17, _size14);
            this->devTokens.resize(_size14);
            uint32_t _i18;
            for (_i18 = 0; _i18 < _size14; ++_i18)
            {
              xfer += iprot->readString(this->devTokens[_i18]);
            }
            xfer += iprot->readListEnd();
          }
          this->__isset.devTokens = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 9:
        if (ftype == ::apache::thrift::protocol::T_BOOL) {
          xfer += iprot->readBool(this->isEnabled);
          this->__isset.isEnabled = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 10:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->quota);
          this->__isset.quota = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 11:
        if (ftype == ::apache::thrift::protocol::T_BOOL) {
          xfer += iprot->readBool(this->devel);
          this->__isset.devel = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t MailAccountInfo::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("MailAccountInfo");

  xfer += oprot->writeFieldBegin("email", ::apache::thrift::protocol::T_STRING, 1);
  xfer += oprot->writeString(this->email);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("mailboxType", ::apache::thrift::protocol::T_STRING, 2);
  xfer += oprot->writeString(this->mailboxType);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("username", ::apache::thrift::protocol::T_STRING, 3);
  xfer += oprot->writeString(this->username);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("password", ::apache::thrift::protocol::T_STRING, 4);
  xfer += oprot->writeString(this->password);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("serverAddress", ::apache::thrift::protocol::T_STRING, 5);
  xfer += oprot->writeString(this->serverAddress);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("serverPort", ::apache::thrift::protocol::T_I32, 6);
  xfer += oprot->writeI32(this->serverPort);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("useSSL", ::apache::thrift::protocol::T_BOOL, 7);
  xfer += oprot->writeBool(this->useSSL);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("devTokens", ::apache::thrift::protocol::T_LIST, 8);
  {
    xfer += oprot->writeListBegin(::apache::thrift::protocol::T_STRING, static_cast<uint32_t>(this->devTokens.size()));
    std::vector<std::string> ::const_iterator _iter19;
    for (_iter19 = this->devTokens.begin(); _iter19 != this->devTokens.end(); ++_iter19)
    {
      xfer += oprot->writeString((*_iter19));
    }
    xfer += oprot->writeListEnd();
  }
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("isEnabled", ::apache::thrift::protocol::T_BOOL, 9);
  xfer += oprot->writeBool(this->isEnabled);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("quota", ::apache::thrift::protocol::T_I32, 10);
  xfer += oprot->writeI32(this->quota);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("devel", ::apache::thrift::protocol::T_BOOL, 11);
  xfer += oprot->writeBool(this->devel);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(MailAccountInfo &a, MailAccountInfo &b) {
  using ::std::swap;
  swap(a.email, b.email);
  swap(a.mailboxType, b.mailboxType);
  swap(a.username, b.username);
  swap(a.password, b.password);
  swap(a.serverAddress, b.serverAddress);
  swap(a.serverPort, b.serverPort);
  swap(a.useSSL, b.useSSL);
  swap(a.devTokens, b.devTokens);
  swap(a.isEnabled, b.isEnabled);
  swap(a.quota, b.quota);
  swap(a.devel, b.devel);
  swap(a.__isset, b.__isset);
}

const char* GenericException::ascii_fingerprint = "3F5FC93B338687BC7235B1AB103F47B3";
const uint8_t GenericException::binary_fingerprint[16] = {0x3F,0x5F,0xC9,0x3B,0x33,0x86,0x87,0xBC,0x72,0x35,0xB1,0xAB,0x10,0x3F,0x47,0xB3};

uint32_t GenericException::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->errorCode);
          this->__isset.errorCode = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->errorMessage);
          this->__isset.errorMessage = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t GenericException::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("GenericException");

  xfer += oprot->writeFieldBegin("errorCode", ::apache::thrift::protocol::T_I32, 1);
  xfer += oprot->writeI32(this->errorCode);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("errorMessage", ::apache::thrift::protocol::T_STRING, 2);
  xfer += oprot->writeString(this->errorMessage);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(GenericException &a, GenericException &b) {
  using ::std::swap;
  swap(a.errorCode, b.errorCode);
  swap(a.errorMessage, b.errorMessage);
  swap(a.__isset, b.__isset);
}

const char* FetchDBUnableToPutItemException::ascii_fingerprint = "D64E8622DC3BED8546558B0ADE7FCD75";
const uint8_t FetchDBUnableToPutItemException::binary_fingerprint[16] = {0xD6,0x4E,0x86,0x22,0xDC,0x3B,0xED,0x85,0x46,0x55,0x8B,0x0A,0xDE,0x7F,0xCD,0x75};

uint32_t FetchDBUnableToPutItemException::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRUCT) {
          xfer += this->item.read(iprot);
          this->__isset.item = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->errorMessage);
          this->__isset.errorMessage = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t FetchDBUnableToPutItemException::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("FetchDBUnableToPutItemException");

  xfer += oprot->writeFieldBegin("item", ::apache::thrift::protocol::T_STRUCT, 1);
  xfer += this->item.write(oprot);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("errorMessage", ::apache::thrift::protocol::T_STRING, 2);
  xfer += oprot->writeString(this->errorMessage);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(FetchDBUnableToPutItemException &a, FetchDBUnableToPutItemException &b) {
  using ::std::swap;
  swap(a.item, b.item);
  swap(a.errorMessage, b.errorMessage);
  swap(a.__isset, b.__isset);
}

} // namespace
