/**
 * Autogenerated by Thrift Compiler (0.9.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#ifndef PMMSuckerRPC_H
#define PMMSuckerRPC_H

#include <thrift/TDispatchProcessor.h>
#include "pmmrpc_types.h"

namespace pmmrpc {

class PMMSuckerRPCIf {
 public:
  virtual ~PMMSuckerRPCIf() {}
  virtual void getAllEmailAccounts(std::vector<std::string> & _return) = 0;
  virtual bool fetchDBPutItem(const std::string& email, const std::string& uid) = 0;
  virtual void fetchDBPutItemAsync(const std::string& email, const std::string& uid) = 0;
  virtual void fetchDBInitialSyncPutItemAsync(const std::string& email, const std::string& uidBatch, const std::string& delim) = 0;
  virtual void fetchDBGetItems(std::vector<FetchDBItem> & _return, const std::string& email) = 0;
  virtual bool notificationPayloadPush() = 0;
  virtual bool commandSubmit(const Command& cmd) = 0;
  virtual bool emailAccountRegister(const MailAccountInfo& m) = 0;
  virtual bool emailAccountUnregister(const std::string& email) = 0;
};

class PMMSuckerRPCIfFactory {
 public:
  typedef PMMSuckerRPCIf Handler;

  virtual ~PMMSuckerRPCIfFactory() {}

  virtual PMMSuckerRPCIf* getHandler(const ::apache::thrift::TConnectionInfo& connInfo) = 0;
  virtual void releaseHandler(PMMSuckerRPCIf* /* handler */) = 0;
};

class PMMSuckerRPCIfSingletonFactory : virtual public PMMSuckerRPCIfFactory {
 public:
  PMMSuckerRPCIfSingletonFactory(const boost::shared_ptr<PMMSuckerRPCIf>& iface) : iface_(iface) {}
  virtual ~PMMSuckerRPCIfSingletonFactory() {}

  virtual PMMSuckerRPCIf* getHandler(const ::apache::thrift::TConnectionInfo&) {
    return iface_.get();
  }
  virtual void releaseHandler(PMMSuckerRPCIf* /* handler */) {}

 protected:
  boost::shared_ptr<PMMSuckerRPCIf> iface_;
};

class PMMSuckerRPCNull : virtual public PMMSuckerRPCIf {
 public:
  virtual ~PMMSuckerRPCNull() {}
  void getAllEmailAccounts(std::vector<std::string> & /* _return */) {
    return;
  }
  bool fetchDBPutItem(const std::string& /* email */, const std::string& /* uid */) {
    bool _return = false;
    return _return;
  }
  void fetchDBPutItemAsync(const std::string& /* email */, const std::string& /* uid */) {
    return;
  }
  void fetchDBInitialSyncPutItemAsync(const std::string& /* email */, const std::string& /* uidBatch */, const std::string& /* delim */) {
    return;
  }
  void fetchDBGetItems(std::vector<FetchDBItem> & /* _return */, const std::string& /* email */) {
    return;
  }
  bool notificationPayloadPush() {
    bool _return = false;
    return _return;
  }
  bool commandSubmit(const Command& /* cmd */) {
    bool _return = false;
    return _return;
  }
  bool emailAccountRegister(const MailAccountInfo& /* m */) {
    bool _return = false;
    return _return;
  }
  bool emailAccountUnregister(const std::string& /* email */) {
    bool _return = false;
    return _return;
  }
};


class PMMSuckerRPC_getAllEmailAccounts_args {
 public:

  PMMSuckerRPC_getAllEmailAccounts_args() {
  }

  virtual ~PMMSuckerRPC_getAllEmailAccounts_args() throw() {}


  bool operator == (const PMMSuckerRPC_getAllEmailAccounts_args & /* rhs */) const
  {
    return true;
  }
  bool operator != (const PMMSuckerRPC_getAllEmailAccounts_args &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const PMMSuckerRPC_getAllEmailAccounts_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class PMMSuckerRPC_getAllEmailAccounts_pargs {
 public:


  virtual ~PMMSuckerRPC_getAllEmailAccounts_pargs() throw() {}


  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _PMMSuckerRPC_getAllEmailAccounts_result__isset {
  _PMMSuckerRPC_getAllEmailAccounts_result__isset() : success(false), ex1(false) {}
  bool success;
  bool ex1;
} _PMMSuckerRPC_getAllEmailAccounts_result__isset;

class PMMSuckerRPC_getAllEmailAccounts_result {
 public:

  PMMSuckerRPC_getAllEmailAccounts_result() {
  }

  virtual ~PMMSuckerRPC_getAllEmailAccounts_result() throw() {}

  std::vector<std::string>  success;
  GenericException ex1;

  _PMMSuckerRPC_getAllEmailAccounts_result__isset __isset;

  void __set_success(const std::vector<std::string> & val) {
    success = val;
  }

  void __set_ex1(const GenericException& val) {
    ex1 = val;
  }

  bool operator == (const PMMSuckerRPC_getAllEmailAccounts_result & rhs) const
  {
    if (!(success == rhs.success))
      return false;
    if (!(ex1 == rhs.ex1))
      return false;
    return true;
  }
  bool operator != (const PMMSuckerRPC_getAllEmailAccounts_result &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const PMMSuckerRPC_getAllEmailAccounts_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _PMMSuckerRPC_getAllEmailAccounts_presult__isset {
  _PMMSuckerRPC_getAllEmailAccounts_presult__isset() : success(false), ex1(false) {}
  bool success;
  bool ex1;
} _PMMSuckerRPC_getAllEmailAccounts_presult__isset;

class PMMSuckerRPC_getAllEmailAccounts_presult {
 public:


  virtual ~PMMSuckerRPC_getAllEmailAccounts_presult() throw() {}

  std::vector<std::string> * success;
  GenericException ex1;

  _PMMSuckerRPC_getAllEmailAccounts_presult__isset __isset;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);

};

typedef struct _PMMSuckerRPC_fetchDBPutItem_args__isset {
  _PMMSuckerRPC_fetchDBPutItem_args__isset() : email(false), uid(false) {}
  bool email;
  bool uid;
} _PMMSuckerRPC_fetchDBPutItem_args__isset;

class PMMSuckerRPC_fetchDBPutItem_args {
 public:

  PMMSuckerRPC_fetchDBPutItem_args() : email(), uid() {
  }

  virtual ~PMMSuckerRPC_fetchDBPutItem_args() throw() {}

  std::string email;
  std::string uid;

  _PMMSuckerRPC_fetchDBPutItem_args__isset __isset;

  void __set_email(const std::string& val) {
    email = val;
  }

  void __set_uid(const std::string& val) {
    uid = val;
  }

  bool operator == (const PMMSuckerRPC_fetchDBPutItem_args & rhs) const
  {
    if (!(email == rhs.email))
      return false;
    if (!(uid == rhs.uid))
      return false;
    return true;
  }
  bool operator != (const PMMSuckerRPC_fetchDBPutItem_args &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const PMMSuckerRPC_fetchDBPutItem_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class PMMSuckerRPC_fetchDBPutItem_pargs {
 public:


  virtual ~PMMSuckerRPC_fetchDBPutItem_pargs() throw() {}

  const std::string* email;
  const std::string* uid;

  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _PMMSuckerRPC_fetchDBPutItem_result__isset {
  _PMMSuckerRPC_fetchDBPutItem_result__isset() : success(false), ex1(false), ex2(false) {}
  bool success;
  bool ex1;
  bool ex2;
} _PMMSuckerRPC_fetchDBPutItem_result__isset;

class PMMSuckerRPC_fetchDBPutItem_result {
 public:

  PMMSuckerRPC_fetchDBPutItem_result() : success(0) {
  }

  virtual ~PMMSuckerRPC_fetchDBPutItem_result() throw() {}

  bool success;
  FetchDBUnableToPutItemException ex1;
  GenericException ex2;

  _PMMSuckerRPC_fetchDBPutItem_result__isset __isset;

  void __set_success(const bool val) {
    success = val;
  }

  void __set_ex1(const FetchDBUnableToPutItemException& val) {
    ex1 = val;
  }

  void __set_ex2(const GenericException& val) {
    ex2 = val;
  }

  bool operator == (const PMMSuckerRPC_fetchDBPutItem_result & rhs) const
  {
    if (!(success == rhs.success))
      return false;
    if (!(ex1 == rhs.ex1))
      return false;
    if (!(ex2 == rhs.ex2))
      return false;
    return true;
  }
  bool operator != (const PMMSuckerRPC_fetchDBPutItem_result &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const PMMSuckerRPC_fetchDBPutItem_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _PMMSuckerRPC_fetchDBPutItem_presult__isset {
  _PMMSuckerRPC_fetchDBPutItem_presult__isset() : success(false), ex1(false), ex2(false) {}
  bool success;
  bool ex1;
  bool ex2;
} _PMMSuckerRPC_fetchDBPutItem_presult__isset;

class PMMSuckerRPC_fetchDBPutItem_presult {
 public:


  virtual ~PMMSuckerRPC_fetchDBPutItem_presult() throw() {}

  bool* success;
  FetchDBUnableToPutItemException ex1;
  GenericException ex2;

  _PMMSuckerRPC_fetchDBPutItem_presult__isset __isset;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);

};

typedef struct _PMMSuckerRPC_fetchDBPutItemAsync_args__isset {
  _PMMSuckerRPC_fetchDBPutItemAsync_args__isset() : email(false), uid(false) {}
  bool email;
  bool uid;
} _PMMSuckerRPC_fetchDBPutItemAsync_args__isset;

class PMMSuckerRPC_fetchDBPutItemAsync_args {
 public:

  PMMSuckerRPC_fetchDBPutItemAsync_args() : email(), uid() {
  }

  virtual ~PMMSuckerRPC_fetchDBPutItemAsync_args() throw() {}

  std::string email;
  std::string uid;

  _PMMSuckerRPC_fetchDBPutItemAsync_args__isset __isset;

  void __set_email(const std::string& val) {
    email = val;
  }

  void __set_uid(const std::string& val) {
    uid = val;
  }

  bool operator == (const PMMSuckerRPC_fetchDBPutItemAsync_args & rhs) const
  {
    if (!(email == rhs.email))
      return false;
    if (!(uid == rhs.uid))
      return false;
    return true;
  }
  bool operator != (const PMMSuckerRPC_fetchDBPutItemAsync_args &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const PMMSuckerRPC_fetchDBPutItemAsync_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class PMMSuckerRPC_fetchDBPutItemAsync_pargs {
 public:


  virtual ~PMMSuckerRPC_fetchDBPutItemAsync_pargs() throw() {}

  const std::string* email;
  const std::string* uid;

  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _PMMSuckerRPC_fetchDBPutItemAsync_result__isset {
  _PMMSuckerRPC_fetchDBPutItemAsync_result__isset() : ex1(false), ex2(false) {}
  bool ex1;
  bool ex2;
} _PMMSuckerRPC_fetchDBPutItemAsync_result__isset;

class PMMSuckerRPC_fetchDBPutItemAsync_result {
 public:

  PMMSuckerRPC_fetchDBPutItemAsync_result() {
  }

  virtual ~PMMSuckerRPC_fetchDBPutItemAsync_result() throw() {}

  FetchDBUnableToPutItemException ex1;
  GenericException ex2;

  _PMMSuckerRPC_fetchDBPutItemAsync_result__isset __isset;

  void __set_ex1(const FetchDBUnableToPutItemException& val) {
    ex1 = val;
  }

  void __set_ex2(const GenericException& val) {
    ex2 = val;
  }

  bool operator == (const PMMSuckerRPC_fetchDBPutItemAsync_result & rhs) const
  {
    if (!(ex1 == rhs.ex1))
      return false;
    if (!(ex2 == rhs.ex2))
      return false;
    return true;
  }
  bool operator != (const PMMSuckerRPC_fetchDBPutItemAsync_result &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const PMMSuckerRPC_fetchDBPutItemAsync_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _PMMSuckerRPC_fetchDBPutItemAsync_presult__isset {
  _PMMSuckerRPC_fetchDBPutItemAsync_presult__isset() : ex1(false), ex2(false) {}
  bool ex1;
  bool ex2;
} _PMMSuckerRPC_fetchDBPutItemAsync_presult__isset;

class PMMSuckerRPC_fetchDBPutItemAsync_presult {
 public:


  virtual ~PMMSuckerRPC_fetchDBPutItemAsync_presult() throw() {}

  FetchDBUnableToPutItemException ex1;
  GenericException ex2;

  _PMMSuckerRPC_fetchDBPutItemAsync_presult__isset __isset;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);

};

typedef struct _PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_args__isset {
  _PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_args__isset() : email(false), uidBatch(false), delim(false) {}
  bool email;
  bool uidBatch;
  bool delim;
} _PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_args__isset;

class PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_args {
 public:

  PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_args() : email(), uidBatch(), delim() {
  }

  virtual ~PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_args() throw() {}

  std::string email;
  std::string uidBatch;
  std::string delim;

  _PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_args__isset __isset;

  void __set_email(const std::string& val) {
    email = val;
  }

  void __set_uidBatch(const std::string& val) {
    uidBatch = val;
  }

  void __set_delim(const std::string& val) {
    delim = val;
  }

  bool operator == (const PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_args & rhs) const
  {
    if (!(email == rhs.email))
      return false;
    if (!(uidBatch == rhs.uidBatch))
      return false;
    if (!(delim == rhs.delim))
      return false;
    return true;
  }
  bool operator != (const PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_args &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_pargs {
 public:


  virtual ~PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_pargs() throw() {}

  const std::string* email;
  const std::string* uidBatch;
  const std::string* delim;

  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_result__isset {
  _PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_result__isset() : ex1(false), ex2(false) {}
  bool ex1;
  bool ex2;
} _PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_result__isset;

class PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_result {
 public:

  PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_result() {
  }

  virtual ~PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_result() throw() {}

  FetchDBUnableToPutItemException ex1;
  GenericException ex2;

  _PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_result__isset __isset;

  void __set_ex1(const FetchDBUnableToPutItemException& val) {
    ex1 = val;
  }

  void __set_ex2(const GenericException& val) {
    ex2 = val;
  }

  bool operator == (const PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_result & rhs) const
  {
    if (!(ex1 == rhs.ex1))
      return false;
    if (!(ex2 == rhs.ex2))
      return false;
    return true;
  }
  bool operator != (const PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_result &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_presult__isset {
  _PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_presult__isset() : ex1(false), ex2(false) {}
  bool ex1;
  bool ex2;
} _PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_presult__isset;

class PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_presult {
 public:


  virtual ~PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_presult() throw() {}

  FetchDBUnableToPutItemException ex1;
  GenericException ex2;

  _PMMSuckerRPC_fetchDBInitialSyncPutItemAsync_presult__isset __isset;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);

};

typedef struct _PMMSuckerRPC_fetchDBGetItems_args__isset {
  _PMMSuckerRPC_fetchDBGetItems_args__isset() : email(false) {}
  bool email;
} _PMMSuckerRPC_fetchDBGetItems_args__isset;

class PMMSuckerRPC_fetchDBGetItems_args {
 public:

  PMMSuckerRPC_fetchDBGetItems_args() : email() {
  }

  virtual ~PMMSuckerRPC_fetchDBGetItems_args() throw() {}

  std::string email;

  _PMMSuckerRPC_fetchDBGetItems_args__isset __isset;

  void __set_email(const std::string& val) {
    email = val;
  }

  bool operator == (const PMMSuckerRPC_fetchDBGetItems_args & rhs) const
  {
    if (!(email == rhs.email))
      return false;
    return true;
  }
  bool operator != (const PMMSuckerRPC_fetchDBGetItems_args &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const PMMSuckerRPC_fetchDBGetItems_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class PMMSuckerRPC_fetchDBGetItems_pargs {
 public:


  virtual ~PMMSuckerRPC_fetchDBGetItems_pargs() throw() {}

  const std::string* email;

  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _PMMSuckerRPC_fetchDBGetItems_result__isset {
  _PMMSuckerRPC_fetchDBGetItems_result__isset() : success(false), ex1(false) {}
  bool success;
  bool ex1;
} _PMMSuckerRPC_fetchDBGetItems_result__isset;

class PMMSuckerRPC_fetchDBGetItems_result {
 public:

  PMMSuckerRPC_fetchDBGetItems_result() {
  }

  virtual ~PMMSuckerRPC_fetchDBGetItems_result() throw() {}

  std::vector<FetchDBItem>  success;
  GenericException ex1;

  _PMMSuckerRPC_fetchDBGetItems_result__isset __isset;

  void __set_success(const std::vector<FetchDBItem> & val) {
    success = val;
  }

  void __set_ex1(const GenericException& val) {
    ex1 = val;
  }

  bool operator == (const PMMSuckerRPC_fetchDBGetItems_result & rhs) const
  {
    if (!(success == rhs.success))
      return false;
    if (!(ex1 == rhs.ex1))
      return false;
    return true;
  }
  bool operator != (const PMMSuckerRPC_fetchDBGetItems_result &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const PMMSuckerRPC_fetchDBGetItems_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _PMMSuckerRPC_fetchDBGetItems_presult__isset {
  _PMMSuckerRPC_fetchDBGetItems_presult__isset() : success(false), ex1(false) {}
  bool success;
  bool ex1;
} _PMMSuckerRPC_fetchDBGetItems_presult__isset;

class PMMSuckerRPC_fetchDBGetItems_presult {
 public:


  virtual ~PMMSuckerRPC_fetchDBGetItems_presult() throw() {}

  std::vector<FetchDBItem> * success;
  GenericException ex1;

  _PMMSuckerRPC_fetchDBGetItems_presult__isset __isset;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);

};


class PMMSuckerRPC_notificationPayloadPush_args {
 public:

  PMMSuckerRPC_notificationPayloadPush_args() {
  }

  virtual ~PMMSuckerRPC_notificationPayloadPush_args() throw() {}


  bool operator == (const PMMSuckerRPC_notificationPayloadPush_args & /* rhs */) const
  {
    return true;
  }
  bool operator != (const PMMSuckerRPC_notificationPayloadPush_args &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const PMMSuckerRPC_notificationPayloadPush_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class PMMSuckerRPC_notificationPayloadPush_pargs {
 public:


  virtual ~PMMSuckerRPC_notificationPayloadPush_pargs() throw() {}


  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _PMMSuckerRPC_notificationPayloadPush_result__isset {
  _PMMSuckerRPC_notificationPayloadPush_result__isset() : success(false), ex1(false) {}
  bool success;
  bool ex1;
} _PMMSuckerRPC_notificationPayloadPush_result__isset;

class PMMSuckerRPC_notificationPayloadPush_result {
 public:

  PMMSuckerRPC_notificationPayloadPush_result() : success(0) {
  }

  virtual ~PMMSuckerRPC_notificationPayloadPush_result() throw() {}

  bool success;
  GenericException ex1;

  _PMMSuckerRPC_notificationPayloadPush_result__isset __isset;

  void __set_success(const bool val) {
    success = val;
  }

  void __set_ex1(const GenericException& val) {
    ex1 = val;
  }

  bool operator == (const PMMSuckerRPC_notificationPayloadPush_result & rhs) const
  {
    if (!(success == rhs.success))
      return false;
    if (!(ex1 == rhs.ex1))
      return false;
    return true;
  }
  bool operator != (const PMMSuckerRPC_notificationPayloadPush_result &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const PMMSuckerRPC_notificationPayloadPush_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _PMMSuckerRPC_notificationPayloadPush_presult__isset {
  _PMMSuckerRPC_notificationPayloadPush_presult__isset() : success(false), ex1(false) {}
  bool success;
  bool ex1;
} _PMMSuckerRPC_notificationPayloadPush_presult__isset;

class PMMSuckerRPC_notificationPayloadPush_presult {
 public:


  virtual ~PMMSuckerRPC_notificationPayloadPush_presult() throw() {}

  bool* success;
  GenericException ex1;

  _PMMSuckerRPC_notificationPayloadPush_presult__isset __isset;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);

};

typedef struct _PMMSuckerRPC_commandSubmit_args__isset {
  _PMMSuckerRPC_commandSubmit_args__isset() : cmd(false) {}
  bool cmd;
} _PMMSuckerRPC_commandSubmit_args__isset;

class PMMSuckerRPC_commandSubmit_args {
 public:

  PMMSuckerRPC_commandSubmit_args() {
  }

  virtual ~PMMSuckerRPC_commandSubmit_args() throw() {}

  Command cmd;

  _PMMSuckerRPC_commandSubmit_args__isset __isset;

  void __set_cmd(const Command& val) {
    cmd = val;
  }

  bool operator == (const PMMSuckerRPC_commandSubmit_args & rhs) const
  {
    if (!(cmd == rhs.cmd))
      return false;
    return true;
  }
  bool operator != (const PMMSuckerRPC_commandSubmit_args &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const PMMSuckerRPC_commandSubmit_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class PMMSuckerRPC_commandSubmit_pargs {
 public:


  virtual ~PMMSuckerRPC_commandSubmit_pargs() throw() {}

  const Command* cmd;

  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _PMMSuckerRPC_commandSubmit_result__isset {
  _PMMSuckerRPC_commandSubmit_result__isset() : success(false) {}
  bool success;
} _PMMSuckerRPC_commandSubmit_result__isset;

class PMMSuckerRPC_commandSubmit_result {
 public:

  PMMSuckerRPC_commandSubmit_result() : success(0) {
  }

  virtual ~PMMSuckerRPC_commandSubmit_result() throw() {}

  bool success;

  _PMMSuckerRPC_commandSubmit_result__isset __isset;

  void __set_success(const bool val) {
    success = val;
  }

  bool operator == (const PMMSuckerRPC_commandSubmit_result & rhs) const
  {
    if (!(success == rhs.success))
      return false;
    return true;
  }
  bool operator != (const PMMSuckerRPC_commandSubmit_result &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const PMMSuckerRPC_commandSubmit_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _PMMSuckerRPC_commandSubmit_presult__isset {
  _PMMSuckerRPC_commandSubmit_presult__isset() : success(false) {}
  bool success;
} _PMMSuckerRPC_commandSubmit_presult__isset;

class PMMSuckerRPC_commandSubmit_presult {
 public:


  virtual ~PMMSuckerRPC_commandSubmit_presult() throw() {}

  bool* success;

  _PMMSuckerRPC_commandSubmit_presult__isset __isset;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);

};

typedef struct _PMMSuckerRPC_emailAccountRegister_args__isset {
  _PMMSuckerRPC_emailAccountRegister_args__isset() : m(false) {}
  bool m;
} _PMMSuckerRPC_emailAccountRegister_args__isset;

class PMMSuckerRPC_emailAccountRegister_args {
 public:

  PMMSuckerRPC_emailAccountRegister_args() {
  }

  virtual ~PMMSuckerRPC_emailAccountRegister_args() throw() {}

  MailAccountInfo m;

  _PMMSuckerRPC_emailAccountRegister_args__isset __isset;

  void __set_m(const MailAccountInfo& val) {
    m = val;
  }

  bool operator == (const PMMSuckerRPC_emailAccountRegister_args & rhs) const
  {
    if (!(m == rhs.m))
      return false;
    return true;
  }
  bool operator != (const PMMSuckerRPC_emailAccountRegister_args &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const PMMSuckerRPC_emailAccountRegister_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class PMMSuckerRPC_emailAccountRegister_pargs {
 public:


  virtual ~PMMSuckerRPC_emailAccountRegister_pargs() throw() {}

  const MailAccountInfo* m;

  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _PMMSuckerRPC_emailAccountRegister_result__isset {
  _PMMSuckerRPC_emailAccountRegister_result__isset() : success(false), ex1(false) {}
  bool success;
  bool ex1;
} _PMMSuckerRPC_emailAccountRegister_result__isset;

class PMMSuckerRPC_emailAccountRegister_result {
 public:

  PMMSuckerRPC_emailAccountRegister_result() : success(0) {
  }

  virtual ~PMMSuckerRPC_emailAccountRegister_result() throw() {}

  bool success;
  GenericException ex1;

  _PMMSuckerRPC_emailAccountRegister_result__isset __isset;

  void __set_success(const bool val) {
    success = val;
  }

  void __set_ex1(const GenericException& val) {
    ex1 = val;
  }

  bool operator == (const PMMSuckerRPC_emailAccountRegister_result & rhs) const
  {
    if (!(success == rhs.success))
      return false;
    if (!(ex1 == rhs.ex1))
      return false;
    return true;
  }
  bool operator != (const PMMSuckerRPC_emailAccountRegister_result &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const PMMSuckerRPC_emailAccountRegister_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _PMMSuckerRPC_emailAccountRegister_presult__isset {
  _PMMSuckerRPC_emailAccountRegister_presult__isset() : success(false), ex1(false) {}
  bool success;
  bool ex1;
} _PMMSuckerRPC_emailAccountRegister_presult__isset;

class PMMSuckerRPC_emailAccountRegister_presult {
 public:


  virtual ~PMMSuckerRPC_emailAccountRegister_presult() throw() {}

  bool* success;
  GenericException ex1;

  _PMMSuckerRPC_emailAccountRegister_presult__isset __isset;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);

};

typedef struct _PMMSuckerRPC_emailAccountUnregister_args__isset {
  _PMMSuckerRPC_emailAccountUnregister_args__isset() : email(false) {}
  bool email;
} _PMMSuckerRPC_emailAccountUnregister_args__isset;

class PMMSuckerRPC_emailAccountUnregister_args {
 public:

  PMMSuckerRPC_emailAccountUnregister_args() : email() {
  }

  virtual ~PMMSuckerRPC_emailAccountUnregister_args() throw() {}

  std::string email;

  _PMMSuckerRPC_emailAccountUnregister_args__isset __isset;

  void __set_email(const std::string& val) {
    email = val;
  }

  bool operator == (const PMMSuckerRPC_emailAccountUnregister_args & rhs) const
  {
    if (!(email == rhs.email))
      return false;
    return true;
  }
  bool operator != (const PMMSuckerRPC_emailAccountUnregister_args &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const PMMSuckerRPC_emailAccountUnregister_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class PMMSuckerRPC_emailAccountUnregister_pargs {
 public:


  virtual ~PMMSuckerRPC_emailAccountUnregister_pargs() throw() {}

  const std::string* email;

  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _PMMSuckerRPC_emailAccountUnregister_result__isset {
  _PMMSuckerRPC_emailAccountUnregister_result__isset() : success(false), ex1(false) {}
  bool success;
  bool ex1;
} _PMMSuckerRPC_emailAccountUnregister_result__isset;

class PMMSuckerRPC_emailAccountUnregister_result {
 public:

  PMMSuckerRPC_emailAccountUnregister_result() : success(0) {
  }

  virtual ~PMMSuckerRPC_emailAccountUnregister_result() throw() {}

  bool success;
  GenericException ex1;

  _PMMSuckerRPC_emailAccountUnregister_result__isset __isset;

  void __set_success(const bool val) {
    success = val;
  }

  void __set_ex1(const GenericException& val) {
    ex1 = val;
  }

  bool operator == (const PMMSuckerRPC_emailAccountUnregister_result & rhs) const
  {
    if (!(success == rhs.success))
      return false;
    if (!(ex1 == rhs.ex1))
      return false;
    return true;
  }
  bool operator != (const PMMSuckerRPC_emailAccountUnregister_result &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const PMMSuckerRPC_emailAccountUnregister_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _PMMSuckerRPC_emailAccountUnregister_presult__isset {
  _PMMSuckerRPC_emailAccountUnregister_presult__isset() : success(false), ex1(false) {}
  bool success;
  bool ex1;
} _PMMSuckerRPC_emailAccountUnregister_presult__isset;

class PMMSuckerRPC_emailAccountUnregister_presult {
 public:


  virtual ~PMMSuckerRPC_emailAccountUnregister_presult() throw() {}

  bool* success;
  GenericException ex1;

  _PMMSuckerRPC_emailAccountUnregister_presult__isset __isset;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);

};

class PMMSuckerRPCClient : virtual public PMMSuckerRPCIf {
 public:
  PMMSuckerRPCClient(boost::shared_ptr< ::apache::thrift::protocol::TProtocol> prot) :
    piprot_(prot),
    poprot_(prot) {
    iprot_ = prot.get();
    oprot_ = prot.get();
  }
  PMMSuckerRPCClient(boost::shared_ptr< ::apache::thrift::protocol::TProtocol> iprot, boost::shared_ptr< ::apache::thrift::protocol::TProtocol> oprot) :
    piprot_(iprot),
    poprot_(oprot) {
    iprot_ = iprot.get();
    oprot_ = oprot.get();
  }
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> getInputProtocol() {
    return piprot_;
  }
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> getOutputProtocol() {
    return poprot_;
  }
  void getAllEmailAccounts(std::vector<std::string> & _return);
  void send_getAllEmailAccounts();
  void recv_getAllEmailAccounts(std::vector<std::string> & _return);
  bool fetchDBPutItem(const std::string& email, const std::string& uid);
  void send_fetchDBPutItem(const std::string& email, const std::string& uid);
  bool recv_fetchDBPutItem();
  void fetchDBPutItemAsync(const std::string& email, const std::string& uid);
  void send_fetchDBPutItemAsync(const std::string& email, const std::string& uid);
  void recv_fetchDBPutItemAsync();
  void fetchDBInitialSyncPutItemAsync(const std::string& email, const std::string& uidBatch, const std::string& delim);
  void send_fetchDBInitialSyncPutItemAsync(const std::string& email, const std::string& uidBatch, const std::string& delim);
  void recv_fetchDBInitialSyncPutItemAsync();
  void fetchDBGetItems(std::vector<FetchDBItem> & _return, const std::string& email);
  void send_fetchDBGetItems(const std::string& email);
  void recv_fetchDBGetItems(std::vector<FetchDBItem> & _return);
  bool notificationPayloadPush();
  void send_notificationPayloadPush();
  bool recv_notificationPayloadPush();
  bool commandSubmit(const Command& cmd);
  void send_commandSubmit(const Command& cmd);
  bool recv_commandSubmit();
  bool emailAccountRegister(const MailAccountInfo& m);
  void send_emailAccountRegister(const MailAccountInfo& m);
  bool recv_emailAccountRegister();
  bool emailAccountUnregister(const std::string& email);
  void send_emailAccountUnregister(const std::string& email);
  bool recv_emailAccountUnregister();
 protected:
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> piprot_;
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> poprot_;
  ::apache::thrift::protocol::TProtocol* iprot_;
  ::apache::thrift::protocol::TProtocol* oprot_;
};

class PMMSuckerRPCProcessor : public ::apache::thrift::TDispatchProcessor {
 protected:
  boost::shared_ptr<PMMSuckerRPCIf> iface_;
  virtual bool dispatchCall(::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, const std::string& fname, int32_t seqid, void* callContext);
 private:
  typedef  void (PMMSuckerRPCProcessor::*ProcessFunction)(int32_t, ::apache::thrift::protocol::TProtocol*, ::apache::thrift::protocol::TProtocol*, void*);
  typedef std::map<std::string, ProcessFunction> ProcessMap;
  ProcessMap processMap_;
  void process_getAllEmailAccounts(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
  void process_fetchDBPutItem(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
  void process_fetchDBPutItemAsync(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
  void process_fetchDBInitialSyncPutItemAsync(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
  void process_fetchDBGetItems(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
  void process_notificationPayloadPush(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
  void process_commandSubmit(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
  void process_emailAccountRegister(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
  void process_emailAccountUnregister(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
 public:
  PMMSuckerRPCProcessor(boost::shared_ptr<PMMSuckerRPCIf> iface) :
    iface_(iface) {
    processMap_["getAllEmailAccounts"] = &PMMSuckerRPCProcessor::process_getAllEmailAccounts;
    processMap_["fetchDBPutItem"] = &PMMSuckerRPCProcessor::process_fetchDBPutItem;
    processMap_["fetchDBPutItemAsync"] = &PMMSuckerRPCProcessor::process_fetchDBPutItemAsync;
    processMap_["fetchDBInitialSyncPutItemAsync"] = &PMMSuckerRPCProcessor::process_fetchDBInitialSyncPutItemAsync;
    processMap_["fetchDBGetItems"] = &PMMSuckerRPCProcessor::process_fetchDBGetItems;
    processMap_["notificationPayloadPush"] = &PMMSuckerRPCProcessor::process_notificationPayloadPush;
    processMap_["commandSubmit"] = &PMMSuckerRPCProcessor::process_commandSubmit;
    processMap_["emailAccountRegister"] = &PMMSuckerRPCProcessor::process_emailAccountRegister;
    processMap_["emailAccountUnregister"] = &PMMSuckerRPCProcessor::process_emailAccountUnregister;
  }

  virtual ~PMMSuckerRPCProcessor() {}
};

class PMMSuckerRPCProcessorFactory : public ::apache::thrift::TProcessorFactory {
 public:
  PMMSuckerRPCProcessorFactory(const ::boost::shared_ptr< PMMSuckerRPCIfFactory >& handlerFactory) :
      handlerFactory_(handlerFactory) {}

  ::boost::shared_ptr< ::apache::thrift::TProcessor > getProcessor(const ::apache::thrift::TConnectionInfo& connInfo);

 protected:
  ::boost::shared_ptr< PMMSuckerRPCIfFactory > handlerFactory_;
};

class PMMSuckerRPCMultiface : virtual public PMMSuckerRPCIf {
 public:
  PMMSuckerRPCMultiface(std::vector<boost::shared_ptr<PMMSuckerRPCIf> >& ifaces) : ifaces_(ifaces) {
  }
  virtual ~PMMSuckerRPCMultiface() {}
 protected:
  std::vector<boost::shared_ptr<PMMSuckerRPCIf> > ifaces_;
  PMMSuckerRPCMultiface() {}
  void add(boost::shared_ptr<PMMSuckerRPCIf> iface) {
    ifaces_.push_back(iface);
  }
 public:
  void getAllEmailAccounts(std::vector<std::string> & _return) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->getAllEmailAccounts(_return);
    }
    ifaces_[i]->getAllEmailAccounts(_return);
    return;
  }

  bool fetchDBPutItem(const std::string& email, const std::string& uid) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->fetchDBPutItem(email, uid);
    }
    return ifaces_[i]->fetchDBPutItem(email, uid);
  }

  void fetchDBPutItemAsync(const std::string& email, const std::string& uid) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->fetchDBPutItemAsync(email, uid);
    }
    ifaces_[i]->fetchDBPutItemAsync(email, uid);
  }

  void fetchDBInitialSyncPutItemAsync(const std::string& email, const std::string& uidBatch, const std::string& delim) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->fetchDBInitialSyncPutItemAsync(email, uidBatch, delim);
    }
    ifaces_[i]->fetchDBInitialSyncPutItemAsync(email, uidBatch, delim);
  }

  void fetchDBGetItems(std::vector<FetchDBItem> & _return, const std::string& email) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->fetchDBGetItems(_return, email);
    }
    ifaces_[i]->fetchDBGetItems(_return, email);
    return;
  }

  bool notificationPayloadPush() {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->notificationPayloadPush();
    }
    return ifaces_[i]->notificationPayloadPush();
  }

  bool commandSubmit(const Command& cmd) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->commandSubmit(cmd);
    }
    return ifaces_[i]->commandSubmit(cmd);
  }

  bool emailAccountRegister(const MailAccountInfo& m) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->emailAccountRegister(m);
    }
    return ifaces_[i]->emailAccountRegister(m);
  }

  bool emailAccountUnregister(const std::string& email) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->emailAccountUnregister(email);
    }
    return ifaces_[i]->emailAccountUnregister(email);
  }

};

} // namespace

#endif
